<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::all();
        return response()->json($projects);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $project = Project::create($request->all());

        if ($project) {
            return response()->json([
                'status' => 'success',
                'message' => 'Project created successfully',
                'data' => $project
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to create project'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $projects = Project::where('id',$id)->first();

        if ($projects) {
            return response()->json([
                'status' => 'success',
                'message' => 'Project found',
                'data' => $projects
            ]);
        }
        else {
            return response()->json([
                'status' => 'error',
                'message' => 'Project not found'
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $projects = Project::where('id',$id)->first();

        if ($projects) {
            $projects->update($request->all());
            return response()->json([
                'status' => 'success',
                'message' => 'Project updated successfully',
                'data' => $projects
            ]);
        }
        else {
            return response()->json([
                'status' => 'error',
                'message' => 'Project not found'
            ]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $projects = Project::where('id',$id)->first();

        if ($projects) {
            $projects->delete();
            return response()->json([
                'status' => 'success',
                'message' => 'Project deleted successfully'
            ]);
        }
        else {
            return response()->json([
                'status' => 'error',
                'message' => 'Project not found'
            ]);
        }
    }
}
