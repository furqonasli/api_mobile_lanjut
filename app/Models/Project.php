<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'judul', 'description','status', 'order', 'price', 'start_date', 'end_date'
    ];
}
