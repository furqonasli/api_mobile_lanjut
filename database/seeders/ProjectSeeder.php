<?php

namespace Database\Seeders;

use App\Models\Project;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // $data = [
        //     'judul' => 'Landing Page company profile',
        //     'description' => 'Landing Page company profile: Aplikasi berbasis web yang menampilkan informasi tentang perusahaan, produk, dan lainnya.',
        //     'status' => 'Diproses',
        //     'order' => 'Rio Fernando',
        //     'price' => 'Rp. 20.000.000',   
        //     'start_date' => '2022-07-09',
        //     'end_date' => '2022-12-29',
        // ];

        Project::create([
            'judul' => 'Landing Page company profile',
            'description' => 'Landing Page company profile: Aplikasi berbasis web yang menampilkan informasi tentang perusahaan, produk, dan lainnya.',
            'status' => 'Diproses',
            'order' => 'Rio Fernando',
            'price' => 'Rp. 20.000.000',   
            'start_date' => '2022-07-09',
            'end_date' => '2022-12-29',
        ]);
    }
}
